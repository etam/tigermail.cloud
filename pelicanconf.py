# https://m1nhav02.wordpress.com/2016/05/22/how-to-make-the-page-centered-site-by-pelican/

# Basic
AUTHOR = 'Adam Mizerski'
SITENAME = 'TigerMail'
SITEURL = ''

DELETE_OUTPUT_DIRECTORY = True
DISPLAY_CATEGORIES_ON_MENU = False
MARKDOWN = {
    'output_format': 'html5',
}
PATH = 'content'
PAGE_PATHS = ['']
ARTICLE_PATHS = ['blog']
STATIC_PATHS = ['static']

# Url
RELATIVE_URLS = True
ARTICLE_URL = 'blog/{date:%Y}-{date:%m}-{date:%d}-{slug}.html'
ARTICLE_SAVE_AS = 'blog/{date:%Y}-{date:%m}-{date:%d}-{slug}.html'
DRAFT_URL = 'blog/drafts/{slug}.html'
DRAFT_SAVE_AS = 'blog/drafts/{slug}.html'
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
DRAFT_PAGE_URL = 'drafts/{slug}.html'
DRAFT_PAGE_SAVE_AS = 'drafts/{slug}.html'
AUTHOR_SAVE_AS = ''
CATEGORY_URL = 'blog/category/{slug}.html'
CATEGORY_SAVE_AS = 'blog/category/{slug}.html'
TAG_URL = 'blog/tag/{slug}.html'
TAG_SAVE_AS = 'blog/tag/{slug}.html'
ARCHIVES_SAVE_AS = 'blog/archives.html'
AUTHORS_SAVE_AS = ''
CATEGORIES_SAVE_AS = 'blog/categories.html'
TAGS_SAVE_AS = 'blog/tags.html'
INDEX_SAVE_AS = 'blog/index.html'

# Time and Date
TIMEZONE = 'UTC'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'
LOCALE = 'en_US'

# Template pages
DIRECT_TEMPLATES = ['index', 'categories', 'tags', 'archives']

# Feed
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Themes
THEME = 'themes/etam'
LOCAL_MENUITEMS = [
#    ('Blog', 'blog/index.html'),
]
