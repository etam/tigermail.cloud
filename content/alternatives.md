title: Alternatives

## [Dark Mail](https://darkmail.info/)

How it works:

- Decentralized net (with servers and users registered on them, just like
  regular email).
- Mandatory encryption. Servers and users have their own keys.
- Parts of message are encrypted with different keys. Envelope is for mail
  server, message contents for recipient.
- User keys can be stored on sever or owned by users.
- Email spoofing is not possible.

Advantages:

- Designed for easy gradual migration from existing IMAP and SMTP protocols.

Disadvantages:

- No forward secrecy.
- Specification is incomplete and development is slow (it's one person's project
  right now).


## [BitMessage](https://bitmessage.org/)

How it works:

- Distributed (p2p) network.
- Users are identified by checksum of public key.
- Messages are broadcasted (there is a smart algorithm that makes broadcasting
  more efficient than sending to everyone), only recipient can decrypt.
- A small proof-of-work is added to prevent spamming.

Disadvantages:

- No forward secrecy.
- Asynchronous communication is based on good will of other users.


## Mail in [RetroShare](https://retroshare.cc/)

How it works:

- Distributed (p2p) network.
- Uses OpenPGP and TLS standards.
- Messages are broadcasted, but only to "friends".

Disadvantages:

- No forward secrecy.
- Asynchronous communication is based on good will of your "friends".


## Freemail in [Freenet](http://freenetproject.org/)

Some background about Freenet (see [documentation](https://freenetproject.org/pages/documentation.html#understand)):

- Distributed (p2p) network.
- Freenet is a distributed filesystem, where any user can upload a file, it's
  contents are encrypted and distributed in small chunks for storage among many
  other nodes.
- There are multiple ways to refer to data:
    - "Content Hash Key" (CHK) allows for immutable storage, where data is
      identified by checksum.
    - Signed Subspace Key (SSK) allows for updateable storage, where data
      is identified by a public key and version number. Data is signed with
      corresponding private key. Under a single key multiple files can be stored
      as a directory tree.
    - "Updateable Subspace Key" (USK) is the same as SSK, but client
      automatically tries to find latest version.
    - "Keyword Signed Key" (KSK) allows to store unprotected data under an
      arbitrary name. Anyone can change it.
- "Web of Trust" (WoT) is a subsystem, where users are identified by USK keys.

How Freemail works (see [spec](https://github.com/freenet/plugin-Freemail/blob/master/docs/spec/spec.tex)):

- Alice and Bob create their mailboxes by publishing a `mailsite` file under
  their WoT keys. It contains a RSA-4096 public key and a random string named
  `rtsksk`.
- When Alice wants to send her first message to Bob, she creates a "Request To
  Send" (RTS) message which allows to create communication channel. RTS
  contains:
    - USK key pointing to Alice's `mailsite`.
    - Bob's address.
    - Randomly generated SSK private key for channel.
    - Random values `fetchslot` and `sendslot` (description below).
    - Expiration date.
- Alice signs RTS with her mailsite key and encrypts with Bob's mailsite key.
- Alice publishes RTS under KSK key created from Bob's `rtksks` value, date and
  a natural number (Alice iterates it until first unused KSK key is found).
- Bob downloads and decrypts Alice's RTS message.
- Alice and Bob now share a SSK private key used as communication channel.
- Alice sends messages by storing them under filename `i` (like "initiator") and
  Bob under `r` (like "recipient"). As a first version number Alice uses
  `fetchslot` an Bob `sendslot`. Each next version number is derived from
  SHA-256 checksum of previous one.

Advantages:

- Really asynchronous communication.
- Some sort of forward secrecy.

Disadvantages:

- No true forward secrecy.
- Using KSK keys for storing RTS message allows for easy denial of service attack.


## [I2P-Bote](https://github.com/mhatta/i2p.i2p-bote)

How it works (see [techdoc](https://github.com/mhatta/i2p.i2p-bote/blob/master/doc/techdoc.txt)):

- Uses [I2P](https://geti2p.net) anonymizing distributed (p2p) network.
- Users are identified by public key.
- Sending message:
    - Encrypted with recipient key.
    - Divided into 30kb chunks
    - Each chunk stored in Kademlia DHT.
    - DHT keys of all chunks are written into an "index" file.
    - Index file is stored under DHT key equal to SHA-256 sum of recipient
      address (network allows to store multiple files under a signle key).
- Message is stored for 100 days.
- Each chunk of data contains a SHA-256 checksum of "delete authorization"
  packet. This packet is included in the message and encrypted with the receiver
  key. The receiver can use it to request deletion of data from DHT.

Advantages:

- Really asynchronous communication.
- Anonymizing network

Disadvantages:

- No forward secrecy.


## TODO (To Describe)

- [Briar](https://briarproject.org/)
- [LemonMail](https://github.com/DevanaLabs/lemon.email-dApp)
- [Mailchain](https://mailchain.xyz/)
