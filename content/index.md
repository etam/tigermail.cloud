title: TigerMail
save_as: index.html
status: hidden

TigerMail is asynchronous, distributed and quite secure e-mail system.


## Unique features

- It uses Signal protocol for encryption with forward secrecy,
- It's both asynchronous (peers don't need to be online at the same time) and
  distributed (peer-to-peer network),
- It's using already existing distributed networks:
    - [Swarm](https://swarm.ethereum.org/) for data
    - [Ethereum](https://ethereum.org/) for notifications
- It works by exposing POP3 and SMTP sockets, so you can use your favorite email
  client.


## Author

TigerMail was initially created by [Adam 'Etam' Mizerski](https://etam-software.eu) as part of his [master's thesis]({static}/static/tigermail_thesis.pdf) (it's written in Polish; I'm sorry).
