title: Download

## Rust implementation

Doesn't exist yet. But with signal protocol being reimplemented in Rust (as
[libsignal-client](https://github.com/signalapp/libsignal-client)), it should be
possible soon.


## C++ implementation

It works (in principle), but uses deprecated [swarm](https://github.com/ethersphere/swarm)
client. This "old Swarm network" was shut down on 2021-01-25.

It also requires a password to Ethereum key given in plaintext file.

Have fun with it, study how it's implemented, but don't try to use for anything
serious.


### Source

Source code is available at:
[codeberg.org/etam/tigermail](https://codeberg.org/etam/tigermail)

Building from source is a huge PITA, due to
[aleth](https://github.com/ethereum/aleth/) making it very hard to use its
shared libraries separately.
