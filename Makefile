all: relative absolute

relative:
	pelican content -o output_relative -s pelicanconf.py

absolute:
	pelican content -o output_absolute -s publishconf.py

.PHONY: all relative absolute
