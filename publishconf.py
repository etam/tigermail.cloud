import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEDOMAIN = 'tigermail.cloud'
SITEURL = 'https://' + SITEDOMAIN

RELATIVE_URLS = False
FEED_ATOM = 'blog/feeds/atom.xml'
CATEGORY_FEED_ATOM = 'blog/feeds/cat-{slug}.atom.xml'
TAG_FEED_ATOM = 'blog/feeds/tag-{slug}.atom.xml'
